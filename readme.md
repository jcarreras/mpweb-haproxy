# ![La Salle BES](http://jcarreras.es/images/lasalle.png)

# ![screenshot](.screenshot.gif)

# Descripción
-----------------------

Experimenta con `haproxy`, los backends y su panel de `stats`.

En este ejercicio hay instalados dos servidores web, apache y nginx, escuchando en los puertos 8080 y 8090 respectivamente.
Además, hay ya configurado el balanceador `haproxy` preparado para balancear peticiones de un servidor al otro.

Sigue los pasos para experimentar con el funcionamiento de un balanceador de carga.


# Instalación
-----------------------

```
$ make up
```


# Instrucciones
-----------------------


### Probando el balanceo

- Entra en `http://localhost:8081` para ver el contenido servido por Apache
- Entra en `http://localhost:8082` para ver el contenido servido por Nginx
- Imaginemos que la aplicación es la misma. Con fines didácticos cada servidor muestra en el HTML su nombre, para poder distinguir qué servidor está devolviendo las peticiones pero en condiciones normales los servidores devolverían el mismo HTML
- Abre el fichero `infrastructure/docker/haproxy/haproxy.cfg` para ver como `haproxy` ha sido configurado. La parte interesante está al final del fichero
- Entra en `http://localhost:8080/` . Recarga varias veces y observa el comportamiento

### Viendo el estado de `haproxy`

- Entra en `http://localhost:8080/haproxy?stats`
- Podrás ver las estadísticas de uso para cada uno de los backends
- Recarga la página `http://localhost:8080/` para ver como los valores de `Statistics Report` aumenta

### Viendo qué pasa al parar uno de los servidores web

- Ejecuta `make stop-apache` para parar el servicio de apache2.
- Recarga la página `http://localhost:8080/` repetidas veces observando el comportamiento
- Entra en http://localhost/haproxy?stats y observa el estado de los backends
- Ejecuta, `make up` para levantar de nuevo el servicio de apache2.
- Entra en `http://localhost:8080/haproxy?stats` y observa el estado de los backends


# Desinstalación
-----------------------

```
$ make down
```
