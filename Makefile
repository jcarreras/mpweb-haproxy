

up:
	docker-compose up -d

down:
	docker-compose down


request-to-apache:
	@docker-compose run --rm curl apache

request-to-nginx:
	@docker-compose run --rm curl nginx

request-to-haproxy:
	@docker-compose run --rm curl haproxy


stop-apache:
	docker-compose stop apache